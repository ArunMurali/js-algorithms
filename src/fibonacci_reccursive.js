const { readInput } = require("./utils/helper.js");
const fibonacci = (n) => {
    if(n <2) return n;
return  fibonacci(n) * fibonacci(n-1)
};
async function main() {
  const n = await readInput("enter the fibonacci number");
  const res = fibonacci(n);
  console.log(res);
}
main();
