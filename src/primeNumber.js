const { readInput } = require("./utils/helper.js");
/**
 * O(n) - time complexity
 */
const isPrime = (n) => {
  if (n < 2) return true;
  for (let i = 2; i < n; i++) {
    if (n % i === 0) return false;
  }
  return true;
};

/**
 * O(sqrt(n)) - time complexity
 */
const isPrimeOptimized = (n) => {
  if (n < 2) return true;
  for (let i = 2; i <= Math.sqrt(n); i++) {
    if (n % i === 0) return false;
  }
  return true;
};



async function main() {
  const n = await readInput("enter the number");
  const res = isPrime(n);
  console.log(res);
}
main();
module.exports = { isPrime, isPrimeOptimized};
