module.exports = countDigit = (n) => {
  let count = 0;
  while (n !== 0) {
    n = Math.floor(n / 10);
    count++;
  }
  return count;
};
function main() {
  const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  readline.question("Enter the digit ", (digit) => {
    const res = countDigit(digit);
    console.log(digit, "has", res, "digits");
    readline.close();
  });
}
main();
