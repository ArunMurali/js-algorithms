/**
 * Find the smallest positive number missing from an unsorted array
 *
 * Examples:
 *
 * Input:  arr[] = {2, 3, 7, 6, 8, -1, -10, 15}
 *  Output: 1
 * Input:  arr[] = { 2, 3, -7, 6, 8, 1, -10, 15 }
 *  Output: 4
 * Input: arr[] = {1, 1, 0, -1, -2}
 *  Output: 2
 *
 */

/**
 * @param {Array} arr
 * @return {Number} smallest positive number missing from an unsorted array
 */
const nonNegativeNumber = (arr) => {
  let arrCpy = arr.sort((a, b) => a - b).slice();
  const n = arrCpy.length;
  let present = new Array(n + 1);
  for (let i = 0; i < n + 1; i++) {
    present[i] = false;
  }
  for (let i = 0; i < n; i++) {
    if (arr[i] > 0 && arr[i] < n) present[arr[i]] = true;
  }
  for (let i = 1; i < n; i++) {
    if (!present[i]) return i;
  }
  return n + 1;
};

module.exports = {
  nonNegativeNumber,
};
