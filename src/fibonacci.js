const { readInput } = require("./utils/helper.js");
const fibonacci = (n) => {
  let fib = [0, 1];
  for (let i = 0; i < n - 2; i++) {
    fib[i + 2] = fib[i] + fib[i + 1];
  }
  return fib;
};
async function main() {
  const n = await readInput("enter the fibonacci number");
  const res = fibonacci(n);
  console.log(res);
}
main();
module.exports = {
  fibonacci
}
