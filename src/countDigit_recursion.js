module.exports = digitLength = (digit, flag = 0) => {
  if (digit == 0) {
    return flag;
  }
  let num = Math.floor(digit / 10);
  return digitLength(num, ++flag);
};

function main() {
  const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  readline.question("Enter the digit ", (digit) => {
    const res = digitLength(digit);
    console.log(digit, "has", res, "digits");
    readline.close();
  });
}
main();
