const { readInput } = require("./utils/helper.js");
const factorial = (n) => {
  if (n == 1 || n == 0) {
    return 1;
  }
  return n * factorial(n - 1);
};
async function main() {
  const n = await readInput("enter the number");
  const res = factorial(n);
  console.log(res);
}
main();
module.exports = {factorial}
