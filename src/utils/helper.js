const readInput = async (question) => {
  const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  return new Promise((resolve, reject) => {
    readline.question(question+"\n", (value) => {
      if (value) {
        readline.close();
        resolve(value);
      }
      reject("no input value");
    });
  });
};
module.exports = {
  readInput,
};
