const { readInput } = require("./utils/helper.js");
const bubbleSortAsc = (arr = []) => {
  let length = arr.length;
  if (arr && length) {
    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length; j++)
        if (arr[i] > arr[i + j]) {
          let temp = arr[i + j];
          arr[i + j] = arr[i];
          arr[i] = temp;
        }
    }
  }
  return arr;
};
const bubbleSortDesc = (arr = []) => {
  let length = arr.length;
  if (arr && length) {
    for (let i = 0; i < length; i++) {
      for (let j = 0; j < length; j++)
        if (arr[i] < arr[i + j]) {
          let temp = arr[i + j];
          arr[i + j] = arr[i];
          arr[i] = temp;
        }
    }
  }
  return arr;
};
async function main() {
  const n = await readInput("enter the array");
  const tempArr = JSON.parse(arr);
  const res = bubbleSortAsc(tempArr);
  console.log(res, "asc sort");
  console.log(bubbleSortDesc(tempArr), "dec sort");
}
main();
module.exports = { bubbleSortDesc, bubbleSortAsc };
