const {nonNegativeNumber} = require('../positiveIntegerArray');
const chai = require('chai'); 
const expect = chai.expect;


describe("smallest positive number missing from an unsorted array",()=>{
it("Test array [2, 3, -7, 6, 8, 1, -10, 15] whose output  is  4",()=>{
    expect(nonNegativeNumber([2, 3, -7, 6, 8, 1, -10, 15])).to.be.equal(4);
});
it("Test array [2, 3, 7, 6, 8, -1, -10, 15] whose output  is  1",()=>{
    expect(nonNegativeNumber([2, 3, 7, 6, 8, -1, -10, 15])).to.be.equal(1);
})
it("Test array [1, 1, 0, -1, -2] whose output  is  2",()=>{
    expect(nonNegativeNumber([1, 1, 0, -1, -2])).to.be.equal(2);
})


})