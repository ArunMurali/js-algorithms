const { powerOfTwo } = require("../powerOfTwo.js");
const chai = require("chai");
const expect = chai.expect;

describe("Test the number is a power of 2", () => {
  it("Test the 6 is a power of 2", () => {
    expect(powerOfTwo(6)).to.be.equal(false);
  });
  it("Test the 3 is a power of 2", () => {
    expect(powerOfTwo(3)).to.be.equal(false);
  });
  it("Test the 8 is a power of 2", () => {
    expect(powerOfTwo(8)).to.be.equal(true);
  });
});
