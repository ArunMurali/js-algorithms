const { assert } = require("chai");
const count = require("../countDigit.js");
const digitLength = require("../countDigit_recursion.js");

describe("test count digit using for loop", function () {
  it("3 digit", () => {
    assert.equal(count(456), 3);
  });
  it("2 digit", () => {
    assert.equal(count(25), 2);
  });
  it("1 digit", () => {
    assert.equal(count(1), 1);
  });
});
describe("test count digit using recursion", function () {
    it("3 digit", () => {
      assert.equal(digitLength(456), 3);
    });
    it("2 digit", () => {
      assert.equal(digitLength(25), 2);
    });
    it("1 digit", () => {
      assert.equal(digitLength(1), 1);
    });
  });
