const { assert } = require("chai");
const { bubbleSortAsc, bubbleSortDesc } = require("../bubbleSort.js");
const digitLength = require("../countDigit_recursion.js");

describe("test sort", function () {
  it("test acceding sort", () => {
    assert.deepEqual(
      bubbleSortAsc([45, 12, 50, 33, 56, 45]),
      [12, 33, 45, 45, 50, 56]
    );
  });
});
