const {fibonacci} = require('../fibonacci.js');
const chai = require('chai'); 
const deepEqual = chai.assert.deepEqual;


describe("Test Fibonacci",()=>{
it("test fib(5)",()=>{
    deepEqual(fibonacci(5),[0,1,1,2,3])
});

})