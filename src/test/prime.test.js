const { isPrime, isPrimeOptimized } = require("../primeNumber.js");
const chai = require("chai");
const expect = chai.expect;

describe("Test the input is a prime number", () => {
  it("Test  5 is a prime number", () => {
    expect(isPrime(5)).to.be.equal(true);
  });
  it("Test  4 is a prime number", () => {
    expect(isPrime(4)).to.be.equal(false);
  });
});
describe("Test the input is a prime number using optimized check", () => {
  it("Test  5 is a prime number", () => {
    expect(isPrimeOptimized(5)).to.be.equal(true);
  });
  it("Test  4 is a prime number", () => {
    expect(isPrimeOptimized(4)).to.be.equal(false);
  });
});
