const {factorial} = require('../factorial');
const chai = require('chai'); 
const expect = chai.expect;

describe("Test Factorial",()=>{
it("Test factorial of 5",()=>{
    expect(factorial(5)).to.be.equal(120);
});

})