const { readInput } = require("./utils/helper.js");
const powerOfTwo = (n) => {
  if (n == 0) return 1;
  return 2 * powerOfTwo(n - 1);
};
async function main() {
  const n = await readInput("enter the fibonacci number");
  const res = powerOfTwo(n);
  console.log(res);
}
main();
module.exports = {
  powerOfTwo  
}
