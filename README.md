# JavaScript Programs


### Table of Content
* [Count Digits using recursion ](./src/countDigit_recursion.js)
* [cont digit using while loop](./src/countDigit.js)
* [read keyboard input](./src/readKeyboardInput.js)
* [Print fibonacci number](./src/fibonacci.js)
* [find the factorial of a number](./src/factorial.js)
* [Prime Number](./src/primeNumber.js)
* [Bubble Sort Ascending and Descending](./src/bubbleSort.js)
* [Find the smallest positive number missing from an unsorted array](#section1)
* [Power of two](./src/powerOfTwo.js)

## To run file
```cmd
npm ci
node src/positiveIntegerArray.js
```

## To Run test file
```cmd 
npm ci 
npm run test
```
### [Find the smallest positive number missing from an unsorted array](./src/positiveIntegerArray.js)
<a id="section1" ></a>
 * Input:  arr[] = {2, 3, 7, 6, 8, -1, -10, 15}
 *  Output: 1
 * Input:  arr[] = { 2, 3, -7, 6, 8, 1, -10, 15 }
 *  Output: 4
 * Input: arr[] = {1, 1, 0, -1, -2}
 *  Output: 2


<!-- # Section 1
some text

# Section 2
some text

# Section 3
some text -->